//Load jQuery library using plain JavaScript
(function(){
  var newscript = document.createElement('script');
     newscript.type = 'text/javascript';
     newscript.async = true;
     newscript.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js';
  (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(newscript);
})();


function cargarvariables(){}
var cid = $( "input[name*='campaignId']" ).val();
var nombre = $( "input[name*='bcastName']" ).val();
var BroadcastID = $( "#broadcast-detail > form > div.details > ul:nth-child(2) > div > div:nth-child(3) > div > div > p" ).text();

var emails = "elicht@apple.com,dduran@apple.com,mlujambio1@8amarketing.com,agamez@apple.com,mpanelo@8amarketing.com,fvazquez@8amarketing.com,jcarabajal@apple.com,hvalls@apple.com";
var direccionestest = "elicht@apple.com,dduran@apple.com,mlujambio1@8amarketing.com,outlooktest@8amarketing.com,entourage@8amarketing.com,entourage2@8amarketing.com,outlooktest2@8amarketing.com,apple.dm.qa@gmail.com,apple.dmqa@yahoo.com,apple.dm.qa@hotmail.com,agamez@apple.com,mpanelo@8amarketing.com,hvalls@apple.com,fvazquez@8amarketing.com,jcarabajal@apple.com";
var url = document.URL;
var subjet = nombre + " " + "- " + cid;
var salto =  escape("\n");
var body="" + salto ;
//alert(subjet);

function descargarArchivos(){
	$( "span[tooltip*='Download'] button" ).click();
	setTimeout(cambiarpagina, 4000);

}

function cambiarpagina(){
	window.location.href = 'https://chatterbox.apple.com/cui/#/broadcasts/' + BroadcastID + '/tests';
}

//body
function variablesBodyMail() {	 
	 body += "Hello all," + salto ;
	 body += "" + salto ;
	 body += "In a few minutes you should receive a PRE PRODUCTION TEST: " + subjet + salto ;
	 body += "" + salto ;
	 body += url + salto ;
	 body += "" + salto ;
	 body += "The subject line will be: " + subjetTest + salto ;
	 body += "" + salto ;
	 body += "The purpose of the PRE PRODUCTION TEST is to ensure:" + salto ;
	 body += "1. The subject line is translated and product names are spelled correctly: " + salto ;
	 body += "2. All image and text edits have been made" + salto ;
	 body += "3. Alt tags are correct, match any copy that is in the image, and are translated correctly" + salto ;
	 body += "4. Image paths are pointing to images.apple.com and are using the right region code folder" + salto ;
	 body += "5. All links work and region code in the link matches the region code for the broadcast" + salto ;
	 body += "6. Chatterbox tracking codes are formatted correctly" + salto ;
	 body += "7. HTML code validates correctly (BBEdit markup check)" + salto ;
	 body += "8. Text version matches HTML version correctly" + salto ;
	 body += "9. HTML, Text, and links have been checked across the ALAC Test Matrix" + salto ;
	 body += "10. Translation for \"My Apple ID\". SPA: Mi Apple ID \/\/ PORT: Meu Apple ID" + salto ;
	 body += "" + salto ;
	 body += "Please check both HTML and Text versions to ensure the email is functioning and displaying correctly. To view the text version in Mac Mail, select menu: View > Message > Plain Text Alternative." + salto ;
	 body += "" + salto ;
	 body += "After this test is approved, we will proceed with production testing. Please refer any issues\/concerns directly to Andrew Gamez (agamez@apple.com)." + salto ;
	 body += "" + salto ;
	 body += "" + salto ;
	 body += "" + salto ;
	 body += "" + salto ;
	 body += "Thanks," + salto ;
	 body += "Andrew Gamez" + salto ;
	 body += "agamez@apple.com" + salto ;
	 body += "+54 11 4139-4721" + salto ;
	 body += "" + salto ;
	 body += "THIS TRANSMISSION MAY BE PRIVILEGED AND MAY CONTAIN CONFIDENTIAL INFORMATION INTENDED ONLY FOR THE PERSON(S) NAMED ABOVE. ANY OTHER DISTRIBUTION, RE-TRANSMISSION, COPYING OR DISCLOSURE IS STRICTLY PROHIBITED. IF YOU HAVE RECEIVED THIS TRANSMISSION IN ERROR, PLEASE NOTIFY ME IMMEDIATELY BY TELEPHONE +54 11 4139-4721 OR RETURN E-MAIL, AND DELETE THIS FILE\/MESSAGE FROM YOUR SYSTEM." + salto ;
	 body += "" + salto ;
}

function redactarMail() {
	document.location.href = "mailto:" + emails + "?subject=PRE PRODUCTION TEST: " + subjet + "&body=" + body; 
}

function llenarEmails(){
	$( "#productionEmailAddresses" ).val(direccionestest);
	$( "#productionEmailAddresses" ).text(direccionestest);

	
}

function HabilitarBotonSave(){
	$('button#save-production').attr("disabled", false);

	$( "#productionEmailAddresses" ).removeClass( "ng-pristine" );
	$( "#productionEmailAddresses" ).addClass( "ng-dirty" );

	$( 'button#save-production' ).click();
	setTimeout(enviar, 4000);
}

function enviar(){
	$( 'button#submit' ).click();
}	


function revisar(){
    var headerElement = $( "body > div.CboxContainer.ng-scope > div > main > div > div > section > row > form > div:nth-child(11) > div > div > p" ).attr("tooltip");

    if (!headerElement) {
        console.log("Element doesn't exist. Restarting function");

        setTimeout(revisar, 2000); 
    } else {
        console.log("variablesBodyMail");
        subjetTest = $( "body > div.CboxContainer.ng-scope > div > main > div > div > section > row > form > div:nth-child(11) > div > div > p" ).attr("tooltip");
        //alert(subjetTest);
		variablesBodyMail()
		//llenarEmails()
		//HabilitarBotonSave()
		console.log("testSubjectLine");
		console.log("redactarMail");
		redactarMail()

		$( "button[ng-click*='sendTest()']" ).text("Adjuntar archivos al email");
		$('#errorMessage').append('<div class="errorMessage">Recuerde adjuntar los archivos al email');
		
        
         
    }
}

console.log("cargarvariables");
cargarvariables()
console.log("cambiarpagina");
descargarArchivos()
console.log("revisar");
revisar()
