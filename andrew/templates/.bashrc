# subwrap
export PATH="/bin/ImageOptim-CLI-1.6.19/bin:/opt/subversion/bin:/usr/local/bin:$HOME/.bin:$PATH"
export PATH="/bin/andrew:/opt/subversion/bin:/usr/local/bin:$HOME/.bin:$PATH"
# export PATH=`ls -dt --color=never /Library/Ruby/Gems/1.8/gems/subwrap* | head -n1`/bin:$PATH
# /subwrap
export LC_CTYPE=en_US.UTF-8

export PATH=/opt/subversion/bin:$PATH
export EDITOR='subl -w' # change to your favorite editor, try textmate: "mate -w"

export SVN_EDITOR=$EDITOR
export STAGE_USER="agamez" # change to your stage username
export EMAIL="agamez@apple.com" # change to yourname@apple.com email address

# applecom respository information
export REPOS="https://sourcebox.apple.com/repos/applecom"
export REGION="alac" # change your region if not US
export TRUNK="$REPOS/$REGION/trunk"
export BRANCH="$REPOS/$REGION/branches"
export DEVELOPMENT="$HOME/Development/applecom/" # change to location of your working copy
export CDEVELOPMENT="$HOME/Development/clean_applecom/" # change to location of your clean copies

export LC_COLLATE='C'
export LC_CTYPE='C'

if [ -f $DEVELOPMENT/us/trunk/us/internal/bin/shared.sh ]; then
	source $DEVELOPMENT/us/trunk/us/internal/bin/shared.sh
fi

# Agregado Andrew
alias trunk_br='cd "$HOME/Development/applecom/alac/trunk/br/"'
alias trunk_mx='cd "$HOME/Development/applecom/alac/trunk/mx/"'
alias trunk_la='cd "$HOME/Development/applecom/alac/trunk/la/"'
alias trunk_lae='cd "$HOME/Development/applecom/alac/trunk/lae/"'
alias trunk_us='cd "$HOME/Development/applecom/us/trunk/us/"'
alias trunk_='cd "$HOME/Development/applecom/alac/trunk/"'

alias webedit_br='cd "$HOME/Development/applecom/alac/branches/webedit/br/"'
alias webedit_mx='cd "$HOME/Development/applecom/alac/branches/webedit/mx/"'
alias webedit_la='cd "$HOME/Development/applecom/alac//branches/webedit/la/"'
alias webedit_lae='cd "$HOME/Development/applecom/branches/webedit/lae/"'
alias webedit_us='cd "$HOME/Development/applecom/us/branches/webedit/us/"'
alias webedit_='cd "$HOME/Development/applecom/alac/branches/webedit/"'

alias cdir='. cambiardirectorio'
alias cg='. cambiargeo'
alias ic='. cambiaric'
# /Agregado Andrew

if [ -f $DEVELOPMENT/trunk/bin/shared.sh ]; then
	source $DEVELOPMENT/trunk/bin/shared.sh
fi
if [ -f $DEVELOPMENT/trunk/us/bin/shared.sh ]; then
	source $DEVELOPMENT/trunk/us/bin/shared.sh
fi
if [ -f $DEVELOPMENT/us/trunk/bin/shared.sh ]; then
	source $DEVELOPMENT/us/trunk/bin/shared.sh
fi
if [ -f $DEVELOPMENT/us/trunk/us/bin/shared.sh ]; then
	source $DEVELOPMENT/us/trunk/us/bin/shared.sh
fi
