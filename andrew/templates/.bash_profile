if [ -f ~/.bashrc ]; then
	source ~/.bashrc
fi

PROMPT_COMMAND='PS1="\[\033[0;33m\][\!]\`if [[ \$? = "0" ]]; then echo "\\[\\033[32m\\]"; else echo "\\[\\033[31m\\]"; fi\`[\u.\h: \`if [[ `pwd|wc -c|tr -d " "` > 18 ]]; then echo "\\W"; else echo "\\w"; fi\`]\$\[\033[0m\] "; echo -ne "\033]0;`hostname -s`:`pwd`\007"'


# export PATH=`ls -dt --color=never /Library/Ruby/Gems/1.8/gems/subwrap* | head -n1`/bin:$PATH
