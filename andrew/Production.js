//Load jQuery library using plain JavaScript
(function(){
  var newscript = document.createElement('script');
     newscript.type = 'text/javascript';
     newscript.async = true;
     newscript.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js';
  (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(newscript);
})();


function cargarvariables(){}
var cid = $( "input[name*='campaignId']" ).val();
var nombre = $( "input[name*='bcastName']" ).val();
var BroadcastID = $( "#broadcast-detail > form > div.details > ul:nth-child(2) > div > div:nth-child(3) > div > div > p" ).text();

var emails = "agamez@apple.com,rciordia@apple.com,perdos@apple.com,melissa_contreras@apple.com,ramiro_sanchez@apple.com,elicht@apple.com,dduran@apple.com,mlujambio1@8amarketing.com,mpanelo@8amarketing.com,fvazquez@8amarketing.com,cochoa@apple.com,hvalls@apple.com,aixa@apple.com,ignacio_martinez@apple.com,michelkz@apple.com";
var direccionestest = "alacqa@gmail.com,alacqa@hotmail.com,alacqa@yahoo.com,cbxla@yahoo.com,cochoa@apple.com,dduran@apple.com,elicht@apple.com,fvazquez@8amarketing.com,hvalls@apple.com,jchaves@8amarketing.com,mlujambio1@8amarketing.com,mpanelo@8amarketing.com,outlooktest@8amarketing.com,patierdos@gmail.com,perdos@apple.com,ramiro_sanchez@apple.com,rciordia@apple.com,agamez@apple.com,nestor_gonzalez@apple.com,melissa_contreras@apple.com,melicontreras@gmail.com,juan.garcia@apple.com,ygrossman@apple.com,agamez@apple.com,jpagani@apple.com";
var url = document.URL;
var subjet = nombre + " " + "- " + cid;
var salto =  escape("\n");
var body="" + salto ;
//alert(subjet);

function cambiarpagina(){
window.location.href = 'https://chatterbox.apple.com/cui/#/broadcasts/' + BroadcastID + '/tests';
}

//body
function variablesBodyMail() {	 
	 body += "Hello all," + salto ;
	 body += "" + salto ;
	 body += "In a few minutes you should receive a PRODUCTION TEST: " + subjet + salto ;
	 body += "" + salto ;
	 body += url + salto ;
	 body += "" + salto ;
	 body += "The subject line will be: " + subjetTest + salto ;
	 body += "" + salto ;
	 body += "Please check the HTML and Text versions, and provide your approval on the below items to Rafael Ciordia (rciordia@apple.com)." + salto ;
	 body += "" + salto ;
	 body += "1. Read through both HTML and Text versions to ensure the email is functioning and displaying correctly. This check includes an overall signoff on all copy.  To view the text version in Mac Mail, select menu: View > Message > Plain Text Alternative" + salto ;
	 body += "" + salto ;
	 body += "2. Check that the subject line is translated and reads correctly" + salto ;
	 body += "" + salto ;
	 body += "3. Ensure that all links work, go to the correct landing page, and the region code in the link matches the region code for the broadcast" + salto ;
	 body += "" + salto ;
	 body += "4. Ensure that the Text version matches HTML version" + salto ;
	 body += "" + salto ;
	 body += "5. Platform check - Check designated platform to ensure the email is functioning and displaying correctly." + salto ;
	 body += "" + salto ;
	 body += "Upon approval, this email will be submitted for final TECHNICAL TESTING and broadcast." + salto ;
	 body += "" + salto ;
	 body += "" + salto ;
	 body += "Thanks," + salto ;
	 body += "Andrew Gamez" + salto ;
	 body += "agamez@apple.com" + salto ;
	 body += "+54 11 4139-4721" + salto ;
	 body += "" + salto ;
	 body += "THIS TRANSMISSION MAY BE PRIVILEGED AND MAY CONTAIN CONFIDENTIAL INFORMATION INTENDED ONLY FOR THE PERSON(S) NAMED ABOVE. ANY OTHER DISTRIBUTION, RE-TRANSMISSION, COPYING OR DISCLOSURE IS STRICTLY PROHIBITED. IF YOU HAVE RECEIVED THIS TRANSMISSION IN ERROR, PLEASE NOTIFY ME IMMEDIATELY BY TELEPHONE +54 11 4139-4721 OR RETURN E-MAIL, AND DELETE THIS FILE\/MESSAGE FROM YOUR SYSTEM." + salto ;
	 body += "" + salto ;
}

function redactarMail() {
document.location.href = "mailto:" + emails + "?subject=PRODUCTION TEST: " + subjet + "&body=" + body; 
}

function llenarEmails(){
document.getElementById("testAddresses").value = direccionestest;
document.getElementById("testAddresses").innerHTML = direccionestest;
}

function HabilitarBotonSave(){
$('input#saveSendTestChanges').attr("disabled", false);
$('button#saveSendTestChanges').attr("disabled", false);
$('input#sendTest').attr("disabled", true);
$('button#sendTest').attr("disabled", true);
}


function revisar(){
    var headerElement = $( "body > div.CboxContainer.ng-scope > div > main > div > div > section > row > form > div:nth-child(11) > div > div > p" ).attr("tooltip");

    if (!headerElement) {
        console.log("Element doesn't exist. Restarting function");

        setTimeout(revisar, 2000); 
    } else {
         console.log("variablesBodyMail");
        subjetTest = $( "body > div.CboxContainer.ng-scope > div > main > div > div > section > row > form > div:nth-child(11) > div > div > p" ).attr("tooltip");
        //alert(subjetTest);
		variablesBodyMail()
		
		console.log("testSubjectLine");
		console.log("redactarMail");
		redactarMail()

    }
}

console.log("cargarvariables");
cargarvariables()
console.log("cambiarpagina");
cambiarpagina()
console.log("revisar");
revisar()
