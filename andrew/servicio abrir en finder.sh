trunk="~/Development/applecom/alac/trunk" # Trunk
webedit="~/Development/applecom/alac/branches/webedit" # Webedit
alac=~/Development/applecom/alac # alac
us=~/Development/applecom/us # US
rutaneta="$alac/trunk"

case "$@" in 
  	*ictrunk*|*www*)
		#echo "estas en ictrunk revisado por case"
    	rutaneta="$alac/trunk"
    	arbol="trunk"
    ;;
  	*webedit*)
		#echo "estas en webedit revisado por case"
    	rutaneta="$alac/branches/webedit"
    	arbol="branches/webedit"
    ;;
  	*ic[0-9][0-9]*)
		var=$@; var=${var#*ic}; variableic=ic${var:0:2}; 
    	echo "estas en $variableic revisado por case"
    	rutaneta="$alac/branches/$variableic"
    	arbol="branches/$variableic"
    ;;
    
	*)
    	echo "No se encuentra en una rama valida"
    	echo "$rutabruta"
    	
    ;;
esac

if [[ $@ == *mx\/* ]] || [[ $@ == *la\/* ]] ||  [[ $@ == *lae\/* ]] || [[ $@ == *br\/* ]]; then
		producto+=$(echo "$rutaneta")
else
		producto+=$(echo "$us/$arbol/us")
fi

var=$(echo $@ | sed 's/.*apple.com//');
echo $var;

if [[ $var == *html ]] || [[ $var == *php ]]; then
	vdirectorio="${var%/*}"
	if [ -d "$producto$vdirectorio" ]; then
		open -a finder $producto$vdirectorio;
	else
		osascript -e 'tell app "System Events" to display dialog "No se encuentra la carpeta"'
		exit;
	fi
		
else
	if [ -d "$producto$var" ]; then
		open -a finder $producto$var;
	else
		osascript -e 'tell app "System Events" to display dialog "No se encuentra la carpeta"'
		exit;
	fi
fi